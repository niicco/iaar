var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

//gulp
var gulp=require('gulp');
var yaml = require('gulp-yaml');

gulp.src('./src/*.yml')
  .pipe(yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
  .pipe(gulp.dest('./dist/'));

gulp.src('./src/*.yml')
  .pipe(yaml({ space: 2 }))
  .pipe(gulp.dest('./dist/'));

gulp.src('./src/*.yml')
  .pipe(yaml({ safe: true }))
  .pipe(gulp.dest('./dist/'));

//leer yml
yaml = require('js-yaml');
fs   = require('fs');
var minombre;
// Get document, or throw exception on error
try {
  var doc = yaml.safeLoad(fs.readFileSync('contenido.yml', 'utf8'));
  minombre=doc.nosotros.nico;
  console.log(minombre);
} catch (e) {
  console.log(e);
}
//
module.exports = app;
app.listen(3000);


/*medium
var medium = require('medium-sdk')

var client = new medium.MediumClient({
  clientId: 'IAAR',
  clientSecret: '26deb774c2368b6691bba0b46320c660e79c0634c1393f933974f3629ebf6c013'
})

var redirectURL = 'https://yoursite.com/callback/medium';

var url = client.getAuthorizationUrl('secretState', redirectURL, [
  medium.Scope.BASIC_PROFILE, medium.Scope.PUBLISH_POST
])




client.exchangeAuthorizationCode('6deb774c2368b6691bba0b46320c660e79c0634c1393f933974f3629ebf6c013', redirectURL, function (err, token) {
  client.getUser(function (err, user) {
    client.createPost({
      userId: user.id,
      title: 'A new post',
      contentFormat: medium.PostContentFormat.HTML,
      content: '<h1>A New Post</h1><p>This is my new post.</p>',
      publishStatus: medium.PostPublishStatus.DRAFT
    }, function (err, post) {
      console.log(token, user, post)
    })
  })
})
*/
