var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/nosotros', function(req, res, next) {
  res.render('nosotros', { title: 'Express' });
});
router.get('/recursos', function(req, res, next) {
  res.render('recursos', { title: 'Express' });
});
router.get('/recomendaciones', function(req, res, next) {
  res.render('recomendaciones', { title: 'Express' });
});
router.get('/noticias', function(req, res, next) {
  res.render('noticias', { title: 'Express' });
});
router.get('/clusters', function(req, res, next) {
  res.render('clusters', { title: 'Express' });
});
module.exports = router;
